createdb -E UTF8 -O osm osm
echo "CREATE EXTENSION postgis;" | psql -d osm
echo "ALTER TABLE geometry_columns OWNER TO osm;" | psql -d osm
echo "ALTER TABLE spatial_ref_sys OWNER TO osm;" | psql -d osm
echo "ALTER USER osm WITH PASSWORD 'osm';" |psql -d osm
set +x
