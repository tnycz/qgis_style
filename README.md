Pliki dla Qgis oraz imposm3.

create-db.sh
---------------
Skrypt szablonu bazy danych postgres - nazwa bazy: osm, użytkownik: osm. 

imposm3.sh
---------------
Skrypt odpalający imposm3 z mapping.json, do bazy osm na localhost, z pliku pl.pbf - wszystko musi być w katalogu roboczym imposm'a

mapping.json
---------------
Własny mapping dostosowany do pracy z stylami mstb oraz pl100k.

Pliki QML wymagają minimum QGIS 2.0.1

Licencje:
------------------------------------------------
Pliki /pl100k/dw_shield.svg i /pl100k/dk_shield.svg są pracą pochodną, opartą na wikipedii - http://pl.wikipedia.org/wiki/Plik:DW100-PL.svg

